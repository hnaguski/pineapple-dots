#================#
# basic settings #
#================#

# use emacs bindings
bindkey -e

# set path to include ~/.local/bin
export PATH=$HOME/.local/bin:$PATH

#=========#
# history #
#=========#

# file where history is saved
HISTFILE=~/.histfile
# size of history stored in shell
HISTSIZE=10000
# size of history in history file
SAVEHIST=10000

#=======================#
# environment variables #
#=======================#

# set standard editor to nvim
export EDITOR='nvim'

# set terminal to st (non standard, i3 uses it)
export TERMINAL='st'

#=========#
# options #
#=========#

# dont insert the first match immediately when requesting completion 
setopt auto_menu

# complete words from both ends
setopt complete_in_word

# always move cursor to the end of a word after completion
setopt always_to_end

# change directory without cd
setopt autocd

# append history as soon as a command gets run
setopt inc_append_history

# use special characters (~,#,^) as patterns for filename generation 
setopt extendedglob

# report status of background jobs instead of waiting just before printing a prompt
setopt notify

# dont beep
setopt nobeep

#============#
# completion #
#============#

# highlight current selection
zstyle ':completion:*' menu select

# enable colors in completion
zstyle ':completion:*' list-colors ''

# case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# start completion system
zstyle :compinstall filename '/home/henry/.zshrc'
autoload -Uz compinit
compinit

#=========#
# plugins #
#=========#

# zsh-autosuggestions, it suggests commands as you type based on history and
# completions.
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# Pressing escape twice runs the previous command as sudo
source $HOME/.local/share/zsh/plugins/sudo.zsh

# zsh-syntax-highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# git repository status in prompt
source $HOME/.local/share/zsh/plugins/gitstatus/gitstatus.prompt.zsh

# zsh-history-substring-search
source $HOME/.local/share/zsh/plugins/zsh-history-substring-search.zsh

# set bindings for zsh-history-substring-search
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

#================#
# other settings #
#================#

# set the prompt to show the working directory with a '>' after it, this also
# includes the status of any git repositories the user is in.
PROMPT='%~ $GITSTATUS_PROMPT>'

#=========#
# aliases #
#=========#

# multiple .. to cd up multiple directories at once
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

# ls colors
alias ls="ls --color=auto"

# la to list all files in a vertical list
alias la="ls -lah"

# c to clear terminal
alias c="clear"

# mixer to open pulsemixer
alias mixer="pulsemixer"

# get the weather 
alias weather="curl 'wttr.in/?Fq'"

# get the weather, smaller
alias wtr="curl 'wttr.in/?Fqn2'"

# get the moon phase
alias moon="curl 'wttr.in/moon?F'"

# sleep to suspend with systemctl
alias sleep="systemctl suspend"


