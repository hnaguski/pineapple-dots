# ARCHIVED
## SEE https://git.sr.ht/~hnaguski/pineapple-dots
# pineapple-dots

All of my configuration files for my desktop. Most of the programs and files
are managed with GNU [`stow`](https://www.gnu.org/software/stow/) which
facilitates easy management of everything.

The 'config dependencies' section of each program is a list of all the programs
that are needed for each thing to work correctly with these config files. It is
not necessarily the dependencies for the actual program.

# contents

[program list](#programs)

[other files](#others)

[screenshots](#screenshots)

# programs

## dmenu

[`dmenu`](https://tools.suckless.org/dmenu/) is a menu program for X. It can be
used to launch programs, for a url bar for web browsers, for clipboard
management, and much more. Currently I only really use it to launch programs
inside i3.

#### config dependencies

* `dprint` for using dmenu to launch programs 

#### install:

`# make clean install`

## dprint
[`dprint`](https://git.sr.ht/~kota/dprint) is a program that I use to get a
list of options for my main dmenu script.

## i3

[`i3`](https://i3wm.org/) is a lightweight window manager which is quite nice
to use. I've been using it for quite a while and probably won't switch for a
while more.

#### config dependencies:

* `hsetroot` for wallpaper setting
* `scrot` for screenshots
* `st` for terminal (in this repo)
* `dmenu` for launching programs (in this repo)
* `terminus` for the font
* `mpc` optional, for controlling music with the keyboard
* `pulsemixer` optional, for controlling system volume
* `steam` optional, but has options set for it
* `lxappearance` optional, but has options set for it
* `aerc` optional, it is set to open upon startup
* `keepassxc` optional, it is set to open upon startup
* `wire-messenger` optional, it is set to open upon startup

#### install:

`$ stow i3 -t $HOME`

## mpd

[`mpd`](https://www.musicpd.org/) is somewhat self explanatory, it is a music
player daemon. `mpd` is a server-side program to serve music to a client.

#### install:

`$ stow mpd -t $HOME`

## ncmpcpp

[`ncmpcpp`](https://rybczak.net/ncmpcpp/) stands for NCurses Music Player
Client (Plus Plus). It is an ncurses mpd client which is customizable and
lightweight.

#### config dependencies:

`mpd`

#### install:

`$ stow ncmpcpp -t $HOME`

## neofetch

[`neofetch`](https://github.com/dylanaraps/neofetch) is a command line system
information program. It is highly customizable to display just about any
information about the system. 

#### install:

`$ stow neofetch -t $HOME`

## nvim

[`neovim`](https://neovim.io/), or `nvim`, is a fork of `vim` which is designed
to be extremely extensible with very nice default settings.

#### config dependencies

* `pandoc`, for rendering files as pdfs
* `texlive`, for rendering files as pdfs
* `zathura`, for opening said files from vim

#### install:

`$ stow nvim -t $HOME`

`$ git clone https://github.com/VundleVim/Vundle.vim.git
~/.config/nvim/bundle/Vundle.vim`

## st

[`st`](https://st.suckless.org), or simple terminal, is a terminal emulator by
the suckless team which is focused on combating the bloat of many other popular
terminals.

#### config dependencies

* terminus font. This is what I use but it can be changed in `config.h`

#### install:

`# make clean install`

## zsh

[`zsh`](https://www.zsh.org/) is a shell which has some really great features,
is very extensible, and very popular.

#### config dependencies:

* `zsh-syntax-highlighting`, avaliable in debian repos
* `zsh-autosuggestions`, avaliable in debian repos
* `zsh-history-substring-search.zsh`, installed manually with stow
* `sudo zsh plugin`, installed manually with stow
* `gitstatus`, installed manually by cloning repo

#### install:

`$ stow zsh -t $HOME`

`$ git clone https://github.com/romkatv/gitstatus.git
~/.local/share/zsh/plugins/gitstatus`


# others

## user-dirs

This file is what specifies the system's xdg user directories. This includes
the default desktop, documents, download, music, pictures, videos, share, and
templates directories.

#### install:

`$ stow user-dirs -t $HOME`

## suckless-patches

This directory is a collection of all of the patches I use for the suckless
programs I use. These patches are used to add features or configuration files
to said programs.

# screenshots

![pic 1](https://paste.cf/a19fea9feca44e76de66c800ae375425c968652d.png)

![pic 2](https://paste.cf/f78d4e1728e3944cb97ee99237fac81454533b72.png)

![pic 3](https://paste.cf/67bb1190d44fcc2e705e279066f958502a988cfc.png)
