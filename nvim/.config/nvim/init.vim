"==============="
" neovim config "
"==============="

" this config uses Vundle so make sure that the repository is in the neovim
" config directory

"========"
" vundle "
"========"

" required for vundle (not sure why)
set nocompatible
filetype off

set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin('~/.config/nvim/bundle')

" let vundle do vundle stuff (required)
Plugin 'VundleVim/Vundle.vim'

"---------"
" plugins "
"---------"

" pressing tab (by default) will intelligently use vim's built in autocompletion
Plugin 'ajh17/VimCompletesMe'

" visual undo tree with :MundoToggle
Plugin 'simnalamburt/vim-mundo'

" use ':tab /' to align things easily
Plugin 'godlygeek/tabular'

" comment stuff easily
Plugin 'tpope/vim-commentary'

" color color codes colorfully
Plugin 'ap/vim-css-color'

" make <C-a> and <C-x> work with dates
Plugin 'tpope/vim-speeddating'

" plugins must be added before the next lines, required by vundle
call vundle#end()
filetype plugin indent on

"===================="
" visual adjustments "
"===================="

" enable syntax highlighting
syntax enable

" make the command window 2 lines tall
set cmdheight=2

" line, column number in statusline
set ruler

" make a line at 80 columns
set colorcolumn=80

" make tabs 4 characters wide
set tabstop=4
set shiftwidth=4

" use utf-8 character encoding
set encoding=utf-8

"--------------"
" line numbers "
"--------------"

" turn linenumbers and relative line numbers on by default
set number relativenumber

" toggle relative line numbers upon entering/leaving insert mode
augroup numbertoggle
	autocmd!
	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
	autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

"=================="
" general settings "
"=================="

" better regex implementation
set magic

" persistent undo so undo history is saved across sections
set undofile
set undodir=~/.config/nvim/undo

" case-insensitive search, except when using capital letters
set ignorecase
set smartcase

" use the mouse everywhere
set mouse=a

" set how close to the bottom/top of the window the cursor will go before scrolling
set scrolloff=8

"=================="
" binding settings "
"=================="

" allow moving to next line when moving past the last character on a line with
" arrow keys
set whichwrap=b,s,<,>,[,]

" make '\n' to temp hide the search results
nmap <leader>n :noh<CR>

" make '\h' toggle showing Hidden characters
nmap <leader>h :set list!<CR>
set listchars=tab:▸\ ,eol:¬

" make '\u' as hotkey to show Undo Tree
nmap <leader>u :MundoToggle<CR>

" use pandoc to render current file
nmap <leader>p :!pandoc % -o %:t:r.pdf<CR>
nmap <leader>P :!zathura %:t:r.pdf &<CR>
